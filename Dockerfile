# Build
FROM alpine AS builder

RUN apk add build-base

COPY main.c .

RUN gcc *.c -O3 -fopenmp -lm -std=c99 -o app


# Run
FROM alpine

RUN apk add libgomp

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY --from=builder app .

ENV OMP_NUM_THREADS=1

ENTRYPOINT ["./app"]

#ifndef _IMPLICIT_H_
#define _IMPLICIT_H_

#include "project2_Mico_Tahiraj_struct.h"

/**
 * @brief      Implements the semi-implicit Euler scheme
 *
 * @param[in]  cfg      The configuration of the problem
 * @param[in]  meta     The metadata of this process
 * @param[in]  start_c  The starting concentrations
 */
void implicit(const struct config cfg, struct metadata meta,
              const double *start_c);

#endif // _IMPLICIT_H_

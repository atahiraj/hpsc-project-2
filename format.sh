#!/bin/bash

find -type f -name "*.[ch]" \
    | xargs clang-format -style=file -i


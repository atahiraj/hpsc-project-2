#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdlib.h>
#include <stdio.h>

/**
 * @brief      Converts the indices of an element of a 2D array to that of a
 *             vector that is the concatenation of the lines of the 2D array.
 *             This function considers the  2D array to be a torus.
 *
 * @param[in]  line    The line index
 * @param[in]  col     The column index
 * @param[in]  length  The length of the 2D array
 * @param[in]  height  The height of the 2D array
 *
 * @return     The corresponding index of that element in the corresponding
 *             vector.
 */
__attribute__((always_inline)) static inline unsigned
idx(long line, long col, const long length, const long height)
{
    if (line > height - 1)
        line -= height;
    else if (line < 0)
        line += height;
    if (col > length - 1)
        col -= length;
    else if (col < 0)
        col += length;

    return (unsigned)(line * length + col);
}

/**
 * @brief      Swaps 2 double pointers
 *
 * @param      p1    The pointer to the first double pointer
 * @param      p2    The pointer to the second double pointer
 */
__attribute__((always_inline)) static inline void swap(double **p1, double **p2)
{
    double *temp = *p1;
    *p1 = *p2;
    *p2 = temp;
}

/**
 * @brief      Computes the starting index of a process
 *
 * @param[in]  rank  The process' rank
 * @param[in]  np    The number of processes that will run concurrently
 * @param[in]  len   The length of the vector to slice
 *
 * @return     The starting index of the process
 */
__attribute__((always_inline)) static inline unsigned
startval(const int rank, const int np, const unsigned len)
{
    return len * rank / np;
}

/**
 * @brief      Computes the ending index of a process
 *
 * @param[in]  rank  The process' rank
 * @param[in]  np    The number of processes that will run concurrently
 * @param[in]  len   The length of the vector to slice
 *
 * @return     The ending index of the process
 */
__attribute__((always_inline)) static inline unsigned
endval(const int rank, const int np, const unsigned len)
{
    return len * (rank + 1) / np;
}

/**
 * @brief      Computes the rank of a process when considering the ranks to be
 *             circular
 *
 * @param[in]  np    The number of processes
 * @param[in]  rank  The rank (may be negative or greater than np - 1)
 *
 * @return     The in-bound rank of the process
 */
__attribute__((always_inline)) static inline int circular_rank(const int np,
                                                               const int rank)
{
    if (rank == -1)
        return np - 1;
    else
        return rank % np;
}

/**
 * @brief      Computes the index of an element of a vector when considering the
 *             vector to be circular
 *
 * @param[in]  index  The index (may be negative or greater than vlen)
 * @param[in]  vlen   The length of the vector
 *
 * @return     The in-bound index
 */
__attribute__((always_inline)) static inline unsigned
circular_idx(const long index, const unsigned vlen)
{
    if (index < 0)
        return (unsigned)(index + (2 * (long)(vlen)));
    else
        return (unsigned)(index % (2 * (long)vlen));
}

/**
 * @brief      Writes the concentrations to a file
 *
 * @param[in]  timestep  The timestep
 * @param[in]  p         A pointer to the concentrations
 * @param[in]  len       The length of the side of the problem's square
 */
__attribute__((always_inline)) static inline void
write_to_file(const unsigned timestep, const double *p, const unsigned len)
{
    char u_name[50], v_name[50];
    sprintf(u_name, "./results/u_%u.dat", timestep);
    sprintf(v_name, "./results/v_%u.dat", timestep);
    FILE *u_fp, *v_fp;
    u_fp = fopen(u_name, "wb");
    v_fp = fopen(v_name, "wb");
    if (u_fp == NULL || v_fp == NULL) {
        perror(NULL);
        exit(1);
    }
    fwrite(&len, sizeof(unsigned), 1, u_fp);
    fwrite(&len, sizeof(unsigned), 1, v_fp);

    for (unsigned k = 0; k < (len * len); ++k) {
        fwrite(&p[2 * k], sizeof(double), 1, u_fp);
        fwrite(&p[2 * k + 1], sizeof(double), 1, v_fp);
    }
    fclose(u_fp);
    fclose(v_fp);
}

#endif // _UTILS_H_

#ifndef _MPI_H_
#define _MPI_H_

#include <mpi.h>
#include "project2_Mico_Tahiraj_utils.h"
#include "project2_Mico_Tahiraj_struct.h"

/**
 * @brief      Communicates a vector with the neighboring processes
 *
 * @param      meta   The metadata of this process
 * @param      data   The pointer to the data the communicate
 * @param[in]  count  The amount of elements to communicate
 * @param[in]  vlen   The length of data
 */
__attribute__((always_inline)) static inline void
mpi_circular_comm(struct metadata *meta, double *data, const unsigned count,
                  const unsigned vlen)
{
    if (meta->np == 1)
        return;

    MPI_Request next_request;
    MPI_Request prev_request;
    MPI_Isend(&data[meta->snd_next_idx], count, MPI_DOUBLE, meta->next_proc, 0,
              MPI_COMM_WORLD, &next_request);
    MPI_Isend(&data[2 * meta->startval], count, MPI_DOUBLE, meta->prev_proc, 0,
              MPI_COMM_WORLD, &prev_request);
    MPI_Recv(&data[meta->rcv_prev_idx], count, MPI_DOUBLE, meta->prev_proc, 0,
             MPI_COMM_WORLD, &meta->status);
    MPI_Recv(&data[meta->rcv_next_idx], count, MPI_DOUBLE, meta->next_proc, 0,
             MPI_COMM_WORLD, &meta->status);
    MPI_Wait(&next_request, &meta->status);
    MPI_Wait(&prev_request, &meta->status);
}

/**
 * @brief      Send a vector to the master process (= of rank 0)
 *
 * @param      meta  The metadata of this process
 * @param      data  The data to send to the master
 * @param[in]  vlen  The length of data
 */
__attribute__((always_inline)) static inline void
mpi_send_to_master(struct metadata *meta, double *data, const unsigned vlen)
{
    if (meta->rank == 0) {
        for (int process = 1; process < meta->np; ++process) {
            int proc_start = 2 * startval(process, meta->np, vlen);
            int proc_end = 2 * endval(process, meta->np, vlen);
            MPI_Recv(&data[proc_start], (proc_end - proc_start), MPI_DOUBLE,
                     process, 1, MPI_COMM_WORLD, &meta->status);
        }
    } else {
        MPI_Send(&data[2 * meta->startval],
                 (2 * meta->endval - 2 * meta->startval), MPI_DOUBLE, 0, 1,
                 MPI_COMM_WORLD);
    }
}

/**
 * @brief      Reduce a variable
 *
 * @param      meta  The metadata of this process
 * @param      data  The variable to reduce
 */
__attribute__((always_inline)) static inline void
mpi_reduce(struct metadata *meta, double *data)
{
    if (meta->rank == 0) {
        double bfr = 0;
        for (int process = 1; process < meta->np; ++process) {
            MPI_Recv(&bfr, 1, MPI_DOUBLE, process, 0, MPI_COMM_WORLD,
                     &meta->status);
            *data += bfr;
        }
        for (int process = 1; process < meta->np; ++process) {
            MPI_Send(data, 1, MPI_DOUBLE, process, 0, MPI_COMM_WORLD);
        }
    } else {
        MPI_Send(data, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD);
        MPI_Recv(data, 1, MPI_DOUBLE, 0, 0, MPI_COMM_WORLD, &meta->status);
    }
}

#endif // _MPI_H_

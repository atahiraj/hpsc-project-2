#include <stdlib.h>
#include <stdio.h>

#include "project2_Mico_Tahiraj_explicit.h"
#include "project2_Mico_Tahiraj_mpi.h"
#include "project2_Mico_Tahiraj_struct.h"
#include "project2_Mico_Tahiraj_utils.h"

/**
 * @brief      Convenience structure meant to contain the current concentrations
 *             and the next ones
 */
struct explicitState {
    double *curr_c;
    double *next_c;
};

void explicit(const struct config cfg, struct metadata meta,
              const double *start_c)
{

    /*==== Preparation =======================================================*/
    
    /*---- Allocating space on the heap --------------------------------------*/

    struct explicitState state;
    state.curr_c = (double *)malloc(cfg.vlen * sizeof(double));
    state.next_c = (double *)malloc(cfg.vlen * sizeof(double));
    if (state.curr_c == NULL || state.next_c == NULL) {
        perror(NULL);
        exit(1);
    }


    /*---- Copying starting concentrations -----------------------------------*/

    for (unsigned k = 0; k < cfg.vlen; ++k) {
        state.curr_c[k] = start_c[k];
    }


    /*---- Writing starting concentrations -----------------------------------*/
 
    if (cfg.S != 0)
        write_to_file(0, state.curr_c, cfg.len);


    /*---- Defining some constants -------------------------------------------*/

    const double c_1 = cfg.dt * cfg.du / (cfg.dx * cfg.dx); // dt * Du / dx^2
    const double c_2 = cfg.dt * cfg.f;
    const double c_3 = cfg.dt * cfg.dv / (cfg.dx * cfg.dx); // dt * Dv / dx^2
    const double c_4 = cfg.dt * (cfg.f + cfg.k);



    /*==== Solving ===========================================================*/

    for (unsigned timestep = 1; timestep < cfg.timesteps; ++timestep) {
#pragma omp parallel for

        /*---- Updating concentrations ---------------------------------------*/
        
        for (unsigned k = meta.startval; k < meta.endval; ++k) {
            unsigned k_u = 2 * k;
            unsigned k_v = k_u + 1;
            double u_k = state.curr_c[k_u];
            double v_k = state.curr_c[k_v];
            double uvv = u_k * v_k * v_k * cfg.dt;

            state.next_c[k_u] =
                u_k +
                c_1 * (state.curr_c[cfg.selec[k_u].down] +
                       state.curr_c[cfg.selec[k_u].up] +
                       state.curr_c[cfg.selec[k_u].right] +
                       state.curr_c[cfg.selec[k_u].left] - 4 * u_k) -
                uvv + c_2 * (1 - u_k);

            state.next_c[k_v] =
                v_k +
                c_3 * (state.curr_c[cfg.selec[k_v].down] +
                       state.curr_c[cfg.selec[k_v].up] +
                       state.curr_c[cfg.selec[k_v].right] +
                       state.curr_c[cfg.selec[k_v].left] - 4 * v_k) +
                uvv - c_4 * v_k;
        }
        swap(&state.curr_c, &state.next_c);


        /*---- Communicating concentrations ----------------------------------*/

        mpi_circular_comm(&meta, state.curr_c, 2 * cfg.len, cfg.sqr_len);


        /*---- Write the concentrations in a file every S timestep -----------*/

        if (cfg.S != 0 && timestep % cfg.S == 0) {
            mpi_send_to_master(&meta, state.curr_c, cfg.sqr_len);
            if (meta.rank == 0)
                write_to_file(timestep, state.curr_c, cfg.len);
        }
    }

    free(state.curr_c);
    free(state.next_c);
}

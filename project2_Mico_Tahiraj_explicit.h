#ifndef _EXPLICIT_H_
#define _EXPLICIT_H_

#include "project2_Mico_Tahiraj_struct.h"

/**
 * @brief      Implements the explicit Euler scheme
 *
 * @param[in]  cfg      The configuration of the problem
 * @param[in]  meta     The metadata of this process
 * @param[in]  start_c  The starting concentrations
 */
void explicit(const struct config cfg, struct metadata meta,
              const double *start_c);

#endif // _EXPLICIT_H_

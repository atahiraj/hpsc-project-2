#include <math.h>
#include <stdlib.h>
#include <stdio.h>

#include "project2_Mico_Tahiraj_explicit.h"
#include "project2_Mico_Tahiraj_implicit.h"
#include "project2_Mico_Tahiraj_struct.h"
#include "project2_Mico_Tahiraj_utils.h"
#include "project2_Mico_Tahiraj_mpi.h"

#define U_CONC (double)1
#define V_CONC_HIGH (double)1
#define V_CONC_LOW (double)0
#define RADIUS (double)0.05

int main(int argc, char **argv)
{
    /*==== Checking input arguments ==========================================*/

    if (argc != 3) {
        fprintf(stderr, "2 arguments must be given");
        exit(1);
    }

    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        perror(NULL);
        exit(1);
    }

    struct config cfg;
    struct metadata meta;


    /*==== Reading parameters from file ======================================*/

    double T;

    if (fscanf(fp, "%lf\n %lf\n %lf\n %lf\n %lf\n %lf\n %lf\n %lf\n %d\n",
               &cfg.dt, &cfg.dx, &T, &cfg.du, &cfg.dv, &cfg.f, &cfg.k, &cfg.r_t,
               &cfg.S) != 9) {
        perror(NULL);
        exit(1);
    }
    fclose(fp);
    cfg.timesteps = (unsigned)(T / cfg.dt);
    cfg.len = (unsigned)(1 / cfg.dx);
    cfg.sqr_len = cfg.len * cfg.len;
    cfg.vlen = cfg.sqr_len * 2;


    /*==== Initializing concentrations and selector ==========================*/

    double *c = (double *)malloc(2 * cfg.sqr_len * sizeof(double));
    cfg.selec =
        (struct selector *)malloc(2 * cfg.sqr_len * sizeof(struct selector));
    if (c == NULL || cfg.selec == NULL) {
        perror(NULL);
        exit(1);
    }

    unsigned center_idx = (cfg.len - 1) / 2;
    for (unsigned k = 0; k < cfg.sqr_len; ++k) {
        unsigned k_u = 2 * k;
        unsigned k_v = k_u + 1;

        unsigned i = (unsigned)k % cfg.len;
        unsigned j = (unsigned)(k - i) / cfg.len;
        if (pow((int)(j - center_idx) * cfg.dx, 2) +
                pow((int)(i - center_idx) * cfg.dx, 2) <=
            pow(RADIUS, 2))
            c[k_v] = V_CONC_HIGH;
        else
            c[k_v] = V_CONC_LOW;

        c[k_u] = U_CONC;
    }

    for (unsigned k = 0; k < 2 * cfg.sqr_len; ++k) {
        long length = 2 * cfg.len;
        long height = cfg.len;
        long j = (long)k % length;
        long i = (long)(k - j) / length;
        cfg.selec[k].up = idx(i - 1, j, length, height);
        cfg.selec[k].down = idx(i + 1, j, length, height);
        cfg.selec[k].left = idx(i, j - 2, length, height);
        cfg.selec[k].right = idx(i, j + 2, length, height);
    }


    /*==== Starting MPI and initializing metadata ============================*/

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &meta.np);
    MPI_Comm_rank(MPI_COMM_WORLD, &meta.rank);

    meta.next_proc = circular_rank(meta.np, meta.rank + 1);
    meta.prev_proc = circular_rank(meta.np, meta.rank - 1);
    meta.startval = startval(meta.rank, meta.np, cfg.sqr_len);
    meta.endval = endval(meta.rank, meta.np, cfg.sqr_len);
    meta.rcv_next_idx = circular_idx(2 * (long)meta.endval, cfg.sqr_len);
    meta.rcv_prev_idx = circular_idx(
        (long)(2 * (long)meta.startval - (long)(2 * cfg.len)), cfg.sqr_len);
    meta.snd_next_idx = circular_idx(
        (long)(2 * (long)meta.endval - (long)(2 * cfg.len)), cfg.sqr_len);


    /*==== Solving ===========================================================*/

    if (argv[2][0] == '0') {
        explicit(cfg, meta, c);
    } else if (argv[2][0] == '1') {
        implicit(cfg, meta, c);
    }

    free(c);
    free(cfg.selec);
    MPI_Finalize();

    return 0;
}

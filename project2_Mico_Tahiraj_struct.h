#ifndef _STRUCT_H_
#define _STRUCT_H_

#include <mpi.h>

/**
 * @brief      Convenience structure to avoid the recomputation of an element's
 *             neighbors (see report)
 */
struct selector {
    unsigned up;
    unsigned down;
    unsigned left;
    unsigned right;
};

/**
 * @brief      Convenience structure containing the configuration of the problem
 */
struct config {
    unsigned timesteps;
    unsigned S;
    double dt;
    double dx;
    double du;
    double dv;
    double f;
    double k;
    double r_t;
    double T;
    unsigned len;
    unsigned sqr_len;
    unsigned vlen;
    struct selector *selec;
};

/**
 * @brief      Convenience structure containing information related to the
 *             process. Mainly used for the MPI parallelization of this program
 */ 
struct metadata {
    MPI_Status status;
    int rank;
    int np;
    int next_proc;
    int prev_proc;
    unsigned startval;
    unsigned endval;
    unsigned rcv_next_idx;
    unsigned rcv_prev_idx;
    unsigned snd_next_idx;
};

#endif // _STRUCT_H_

#!/bin/bash
# Submission script for Lemaitre3
#SBATCH --job-name=impl
#SBATCH --time=03:00:00 # hh:mm:ss
#
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=12
#SBATCH --ntasks-per-node=1
#SBATCH --mem-per-cpu=100 # megabytes
#SBATCH --partition=batch,debug
#
#SBATCH --mail-user=atahiraj@student.ulg.ac.be
#SBATCH --mail-type=ALL
#
#SBATCH --comment=HPSC-P2
#
#SBATCH --output=outputs/speed_impl_threads2.out

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
NB_TASKS=$SLURM_NTASKS 

echo "Number of processes: ${NB_TASKS}"
echo "Number of threads per process: ${OMP_NUM_THREADS}"
echo ""
echo ""

module load OpenMPI/3.1.4-GCC-8.3.0

mpicc -O3 -lm -fopenmp -std=c99 -o test *.c

for t in 1 2 4 8 12
do
	echo "Number of threads: ${t}"
	export OMP_NUM_THREADS=$t
	time mpirun -np $NB_TASKS --bind-to none ./test param.txt 1
	echo ""
done

module unload OpenMPI/3.1.4-GCC-8.3.0

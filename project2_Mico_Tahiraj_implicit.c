#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

#include "project2_Mico_Tahiraj_implicit.h"
#include "project2_Mico_Tahiraj_mpi.h"
#include "project2_Mico_Tahiraj_struct.h"
#include "project2_Mico_Tahiraj_utils.h"

/**
 * @brief      Convenience structure containing all the vectors needed by the
 *             conjugate gradient method
 */
struct implicitState {
    double *c; // Concentrations
    double *x;
    double *r;
    double *p;
    double *bfr; // To update r
};

/**
 * @brief      Computes the norm of a vector
 *
 * @param      meta    The metadata of this process
 * @param[in]  vector  The vector whose length is to be computed
 * @param[in]  vlen    The length of vector
 *
 * @return     The norm of the vector
 */
__attribute__((always_inline)) static inline double
compute_norm(struct metadata *meta, const double *vector, const unsigned vlen)
{
    double norm = 0;
#pragma omp parallel for reduction(+ : norm)
    for (unsigned k = meta->startval; k < meta->endval; ++k) {
        unsigned k_u = 2 * k;
        unsigned k_v = k_u + 1;
        norm += vector[k_u] * vector[k_u] + vector[k_v] * vector[k_v];
    }
    mpi_reduce(meta, &norm);
    return norm;
}

void implicit(const struct config cfg, struct metadata meta,
              const double *start_c)
{

    /*==== Preparation =======================================================*/

    /*---- Allocating space on the heap --------------------------------------*/

    struct implicitState state;
    state.c = (double *)malloc(cfg.vlen * sizeof(double));
    state.x = (double *)malloc(cfg.vlen * sizeof(double));
    state.r = (double *)malloc(cfg.vlen * sizeof(double));
    state.p = (double *)malloc(cfg.vlen * sizeof(double));
    state.bfr = (double *)malloc(cfg.vlen * sizeof(double));


    /*---- Copying starting concentrations -----------------------------------*/

    for (unsigned k = 0; k < cfg.vlen; ++k) {
        state.c[k] = start_c[k];
    }


    /*---- Writing starting concentrations -----------------------------------*/

    if (cfg.S != 0)
        write_to_file(0, state.c, cfg.len);


    /*---- Defining some constants -------------------------------------------*/

    const double u_A_x = -cfg.dt * cfg.du / (cfg.dx * cfg.dx);
    const double v_A_x = -cfg.dt * cfg.dv / (cfg.dx * cfg.dx);
    const double u_A_k = 1 - 4 * u_A_x;
    const double v_A_k = 1 - 4 * v_A_x;
    const double c_1 = cfg.dt * cfg.f;
    const double c_2 = cfg.dt * (cfg.f + cfg.k);


    /*==== Solving ===========================================================*/

    for (unsigned timestep = 1; timestep < cfg.timesteps; ++timestep) {
#pragma omp parallel for

        /*---- Initialazing x, r and p ---------------------------------------*/

        for (unsigned k = meta.startval; k < meta.endval; ++k) {
            unsigned k_u = 2 * k;
            unsigned k_v = k_u + 1;
            double u_k = state.c[k_u];
            double v_k = state.c[k_v];
            double uvv = u_k * v_k * v_k * cfg.dt;

            state.p[k_u] = state.r[k_u] = u_k - uvv + c_1 * (1 - u_k);
            state.p[k_v] = state.r[k_v] = v_k + uvv - c_2 * v_k;

            state.x[k_u] = 0;
            state.x[k_v] = 0;
        }

        
        /*---- Computing loop exit condition ---------------------------------*/

        double r_norm = compute_norm(&meta, state.r, cfg.sqr_len);
        double r_norm_0 = r_norm;
        double exit_condition = cfg.r_t * cfg.r_t * r_norm;
        while (1) { // Should not loop more than twice anyways
            mpi_circular_comm(&meta, state.p, 2 * cfg.len, cfg.sqr_len);
            

            /*---- Computing alpha -------------------------------------------*/

            double alpha = r_norm;
            double denom = 0;
#pragma omp parallel for reduction(+ : denom)
            for (unsigned k = meta.startval; k < meta.endval; ++k) {
                unsigned k_u = 2 * k;
                unsigned k_v = k_u + 1;
                double p_u_k = state.p[k_u];
                double p_v_k = state.p[k_v];
                state.bfr[k_u] =
                    u_A_k * p_u_k + u_A_x * (state.p[cfg.selec[k_u].down] +
                                             state.p[cfg.selec[k_u].up] +
                                             state.p[cfg.selec[k_u].right] +
                                             state.p[cfg.selec[k_u].left]);
                state.bfr[k_v] =
                    v_A_k * p_v_k + v_A_x * (state.p[cfg.selec[k_v].down] +
                                             state.p[cfg.selec[k_v].up] +
                                             state.p[cfg.selec[k_v].right] +
                                             state.p[cfg.selec[k_v].left]);

                denom += p_u_k * state.bfr[k_u] + p_v_k * state.bfr[k_v];
            }
            mpi_reduce(&meta, &denom);
            alpha /= denom;


            /*---- Computing x_{i+1} and r_{i+1} -----------------------------*/

#pragma omp parallel for
            for (unsigned k = meta.startval; k < meta.endval; ++k) {
                unsigned k_u = 2 * k;
                unsigned k_v = k_u + 1;
                state.x[k_u] += alpha * state.p[k_u];
                state.x[k_v] += alpha * state.p[k_v];
                state.r[k_u] -= alpha * state.bfr[k_u];
                state.r[k_v] -= alpha * state.bfr[k_v];
            }


            /*---- Computing beta --------------------------------------------*/

            double new_r_norm = compute_norm(&meta, state.r, cfg.sqr_len);
            if (new_r_norm < exit_condition) // We ùight leave sooner
                break;
            double beta = new_r_norm / r_norm;
            r_norm = new_r_norm;


            /*---- Computing p_{i+1} -----------------------------------------*/

            for (unsigned k = meta.startval; k < meta.endval; ++k) {
                unsigned k_u = 2 * k;
                unsigned k_v = k_u + 1;
                state.p[k_u] = state.r[k_u] + beta * state.p[k_u];
                state.p[k_v] = state.r[k_v] + beta * state.p[k_v];
            }
        }
        swap(&state.c, &state.x);

        /*---- Write the concentrations in a file every S timestep -----------*/

        if (cfg.S != 0 && timestep % cfg.S == 0) {
            mpi_send_to_master(&meta, state.c, cfg.sqr_len);
            if (meta.rank == 0)
                write_to_file(timestep, state.c, cfg.len);
        }
    }

    free(state.c);
    free(state.x);
    free(state.r);
    free(state.p);
    free(state.bfr);
}
